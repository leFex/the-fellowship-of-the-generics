"use strict";

exports.categories = {
    forest: "forest",
    water: "water",
    mountain: "mountain",
    city: "city",
    shortcut: "shortcut"
};

exports.ActionCard = class {

    constructor (category, title, content, options, id) {
        this.class = "action";
        this.category = category;
        this.title = title;
        this.content = content;
        this.options = options;
        this.id = id;
    }
}

exports.GroupCard = class {
    
    constructor (category, title, content, options, id) {
        this.class = "group";
        this.category = category;
        this.title = title;
        this.content = content;
        this.options = options;
        this.id = id;
    }
}

exports.IndividualCard = class {
    constructor (category, title, content, options, id) {
        this.class = "individual";
        this.category = category;
        this.title = title;
        this.content = content;
        this.options = options;
        this.id = id;
    }
}

exports.type = {
    action: exports.ActionCard,
    group: exports.GroupCard,
    individual: exports.IndividualCard
}
