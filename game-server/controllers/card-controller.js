"use strict";

let cards = require('../models/card.js');
let cardDao = require('../dao/card-dao.js');

exports.create_card = function(req, res, next) {

    let c = req.body;
    let card = null;

    try {
        card = new cards.type[req.body.class](c.category, c.title, c.content, c.options)
    }
    catch (error) {
        res.header(400);
        res.send("ERROR - felformaterat kort");
        return;
    }

    cardDao.createCard(card, (error) => {
        if (error) {
            res.header(500);
            res.send("Error - det gick inte att spara kortet");
        }
        else {
            res.header(201);
            res.send(card);
        }
    });   
}

exports.update_card = function(req, res, next) {

    let c = req.body;
    let card = null;

    try {
        card = new cards.type[req.body.class](c.category, c.title, c.content, c.options, c.id)
    }
    catch (error) {
        res.status(400);
        res.send("ERROR - felformaterat kort");
        return;
    }

    cardDao.updateCard(card, (error) => {
        if (error) {
            res.status(500);
            res.send("Error - det gick inte att spara kortet");
        }
        else {
            res.status(200);
            res.send(card);
        }
    });  
}

exports.get_all_cards = function(req, res, next) {
    res.send(cardDao.getAllCards());
}

exports.get_card = function(req, res, next) {
    res.send(cardDao.getCard(req.params.id));
}